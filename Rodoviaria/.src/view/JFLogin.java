package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JPasswordField;
import javax.swing.JButton;

public class JFLogin extends JFrame {

	private JPanel contentPane;
	private JTextField txtUsuario;
	private JPasswordField txtSenha;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					JFLogin frame = new JFLogin();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public JFLogin() {
		setTitle("SisRodovi\u00E1ria- Tela de Login");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 438, 319);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblNewLabel = new JLabel("Sistema Rodovi\u00E1ria - Bem Vindo!");
		lblNewLabel.setFont(new Font("Dubai", Font.BOLD, 25));
		lblNewLabel.setBounds(10, -3, 414, 35);
		contentPane.add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Informe suas credencias de acesso:");
		lblNewLabel_1.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 14));
		lblNewLabel_1.setBounds(20, 33, 283, 35);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Usu\u00E1rio:");
		lblNewLabel_2.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 15));
		lblNewLabel_2.setBounds(30, 74, 65, 35);
		contentPane.add(lblNewLabel_2);
		
		txtUsuario = new JTextField();
		txtUsuario.setBounds(102, 83, 294, 20);
		contentPane.add(txtUsuario);
		txtUsuario.setColumns(10);
		
		JLabel lblNewLabel_3 = new JLabel("Senha:");
		lblNewLabel_3.setFont(new Font("Dialog", Font.BOLD, 15));
		lblNewLabel_3.setBounds(30, 134, 55, 25);
		contentPane.add(lblNewLabel_3);
		
		txtSenha = new JPasswordField();
		txtSenha.setBounds(102, 138, 294, 20);
		contentPane.add(txtSenha);
		
		JButton btnAcessar = new JButton("ACESSAR");
		btnAcessar.setFont(new Font("Dialog", Font.BOLD, 17));
		btnAcessar.setBounds(277, 169, 121, 25);
		contentPane.add(btnAcessar);
		
		JButton btnCancelar = new JButton("CANCELAR");
		btnCancelar.setFont(new Font("Dialog", Font.BOLD, 17));
		btnCancelar.setBounds(123, 169, 134, 25);
		contentPane.add(btnCancelar);
		
		JButton btnCadastrar = new JButton("CADASTRAR-SE");
		btnCadastrar.setFont(new Font("Dialog", Font.BOLD, 14));
		btnCadastrar.setBounds(10, 208, 146, 23);
		contentPane.add(btnCadastrar);
		
		JButton btnRecuperarSenha = new JButton("RECUPERAR SENHA");
		btnRecuperarSenha.setFont(new Font("Dialog", Font.BOLD, 14));
		btnRecuperarSenha.setBounds(166, 209, 181, 23);
		contentPane.add(btnRecuperarSenha);
	}
}
